# Local
terraform {
  backend "local" {
    path = "terraform/state/terraform.tfstate"
  }
}

# Remote
terraform {
  backend "gcs" {
    bucket  = "# REPLACE WITH YOUR BUCKET NAME"
    prefix  = "terraform/state"
  }
}