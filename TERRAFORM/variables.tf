#cuando ingreso la variable, default corresponde al valor
variable "project_id" {
  description = "The project ID to host the network in"
  default = "qwiklabs-gcp-02-7f1399f39581"
}

variable "network_name" {
  description = "The name of the VPC network being created"
  default = "example-vpc"
}

#otro ejemplo de vairables
variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
  default     = "FILL IN YOUR PROJECT ID HERE"
}
variable "name" {
  description = "Name of the buckets to create."
  type        = string
  default     = "FILL IN YOUR (UNIQUE) BUCKET NAME HERE"
}