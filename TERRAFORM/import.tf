# Run the following terraform import command to attach the existing Docker container to the docker_container.web
# The command docker inspect -f {{.ID}} hashicorp-learn returns the full SHA256 container ID:
terraform import docker_container.web $(docker inspect -f {{.ID}} hashicorp-learn)
#Verify that the container has been imported
terraform show
# create Terraform configuration before you can use Terraform to manage this container.
terraform plan
# Terraform will show errors -->two approaches to update the configuration
# 1. Using the current state is often faster
terraform show -no-color > docker.tf
# 2. Individually selecting the required attributes
# You can selectively remove these optional attributes. Remove all of these attributes, keeping only the required attributes: image, name, and ports
terraform plan
terraform apply 
