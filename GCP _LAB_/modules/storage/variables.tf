variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
}

variable "region" {
  description = "region."
  type        = string
}

variable "zone" {
  description = "zone."
  type        = string
}