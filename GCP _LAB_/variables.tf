variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
  default     = "FILL IN  HERE"
}

variable "region" {
  description = "region."
  type        = string
  default     = "FILL IN  HERE"
}

variable "zone" {
  description = "zone."
  type        = string
  default     = "FILL IN  HERE"
}