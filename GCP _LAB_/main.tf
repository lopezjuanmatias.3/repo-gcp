# terraform {} block is required so Terraform knows which provider to download from the Terraform Registry.",
terraform {
    required_providers {
      google = {
        source = "hashicorp/google"
      }
    }
  }
  # The provider block is used to configure the named provider, in this case google
  provider "google" {
    project = var.project_id
    region  = var.region
    zone    = var.zone
  }
  
  # referencia a modulo instances
  module "instances" {
    source = "./modules/instances"
    project_id = var.project_id
}
